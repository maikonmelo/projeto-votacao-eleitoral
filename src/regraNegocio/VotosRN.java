
package regraNegocio;

import java.util.ArrayList;
import pers.VotosPers;
import vo.VotosVO;

/**
 *
 * @author MAIKON
 */
public class VotosRN {
    
    public VotosRN(){
        
    }
    
    public void cadastrarVotos(VotosVO votoVO) throws Exception{
        VotosPers votoPers = new VotosPers();
        votoPers.cadastrarVotos(votoVO);
        
    }
    
    public ArrayList<Integer> votosReitor(){
      
        VotosPers votosReitor = new VotosPers();
        
        ArrayList<Integer> votosR = votosReitor.votosReitor();
        
        return votosR;
        
    }
    
    public ArrayList<Integer> votosChefe(){
      
        VotosPers votosChefe = new VotosPers();
        
        ArrayList<Integer> votosCh = votosChefe.votosChefe();
        
        return votosCh;
        
    }
    
    public ArrayList<Integer> votosCoordenador(){
      
        VotosPers votosCoordenador = new VotosPers();
        
        ArrayList<Integer> votosCoor = votosCoordenador.votosChefe();
        
        return votosCoor;
        
    }
    
    public ArrayList<Integer> votosRepresentante(){
      
        VotosPers votosRepresentante = new VotosPers();
        
        ArrayList<Integer> votosRep = votosRepresentante.votosRepresentante();
        
        return votosRep;
        
    }
    
    public String verificaEleitor (String registro) throws Exception{
        String consulta;
        
        VotosPers verificaRegistro = new VotosPers();
        consulta = verificaRegistro.verificarEleitor(registro);
        
        return consulta;
    }
}
