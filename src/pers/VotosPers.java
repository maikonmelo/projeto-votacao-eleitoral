package pers;

import java.util.ArrayList;
import vo.VotosVO;

/**
 *
 * @author MAIKON
 */
public class VotosPers {

    public VotosPers() {
        
    }

    static int qtdVotos = 0;

    static int chapa1 = 0;
    static int chapa2 = 0;
    static int chapa3 = 0;

    static int chefe1 = 0;
    static int chefe2 = 0;
    static int chefe3 = 0;

    static int coordenador1 = 0;
    static int coordenador2 = 0;
    static int coordenador3 = 0;

    static int representante1 = 0;
    static int representante2 = 0;
    static int representante3 = 0;

    static ArrayList<String> votantes = new ArrayList();

    public void cadastrarVotos(VotosVO votosVO) throws Exception {

        try {
            //Verificar voto para reitor
            if (null != votosVO.getReitor()) {
                switch (votosVO.getReitor()) {
                    case "CHAPA 1":
                        chapa1++;
                        break;
                    case "CHAPA 2":
                        chapa2++;
                        break;
                    case "CHAPA 3":
                        chapa3++;
                        break;
                    default:
                        break;
                }
            }

            //Verificar voto para chefe de departamento
            if (null != votosVO.getChefe()) {
                switch (votosVO.getChefe()) {
                    case "Chefe 1":
                        chefe1++;
                        break;
                    case "Chefe 2":
                        chefe2++;
                        break;
                    case "Chefe 3":
                        chefe3++;
                        break;
                    default:
                        break;
                }
            }

            //Verificar voto para coordenador de curso
            if (null != votosVO.getCoordenador()) {
                switch (votosVO.getCoordenador()) {
                    case "Coordenador 1":
                        coordenador1++;
                        break;
                    case "Coordenador 2":
                        coordenador2++;
                        break;
                    case "Coordenador 3":
                        coordenador3++;
                        break;
                    default:
                        break;
                }
            }

            //Verificar voto para representante de turma
            if (null != votosVO.getRepresentante()) {
                switch (votosVO.getRepresentante()) {
                    case "Representante 1":
                        representante1++;
                        break;
                    case "Representante 2":
                        representante2++;
                        break;
                    case "Representante 3":
                        representante3++;
                        break;
                    default:
                        break;
                }
            }
        } finally {
            System.out.println("Inserido...");
        }

        votantes.add(votosVO.getEleitor());
    }

    public String verificarEleitor(String login) throws Exception {
        String eleitor = "semregistro";
        
        for(String s:votantes){
            if(s.equals(login))
                eleitor = s;
        }
        return eleitor;
    }

    public ArrayList<Integer> votosReitor() {

        ArrayList<Integer> votosReitor = new ArrayList();
        votosReitor.add(chapa1);
        votosReitor.add(chapa2);
        votosReitor.add(chapa3);
        return votosReitor;
    }
    
    public ArrayList<Integer> votosChefe() {

        ArrayList<Integer> votosChefe = new ArrayList();
        votosChefe.add(chefe1);
        votosChefe.add(chefe2);
        votosChefe.add(chefe3);
        return votosChefe;
    }
    
    public ArrayList<Integer> votosCoordenador() {

        ArrayList<Integer> votosCoordenador = new ArrayList();
        votosCoordenador.add(coordenador1);
        votosCoordenador.add(coordenador2);
        votosCoordenador.add(coordenador3);
        return votosCoordenador;
    }
    
    public ArrayList<Integer> votosRepresentante() {

        ArrayList<Integer> votosRepresentante = new ArrayList();
        votosRepresentante.add(representante1);
        votosRepresentante.add(representante2);
        votosRepresentante.add(representante3);
        return votosRepresentante;
    }
}
