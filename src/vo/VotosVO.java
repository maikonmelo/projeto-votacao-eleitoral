
package vo;

/**
 *
 * @author MAIKON
 */
public class VotosVO {
    
    private String eleitor;
    private String reitor;
    private String chefe;
    private String coordenador;
    private String representante;

    public String getEleitor() {
        return eleitor;
    }

    public void setEleitor(String eleitor) {
        this.eleitor = eleitor;
    }
    
    public String getReitor() {
        return reitor;
    }

    public void setReitor(String reitor) {
        this.reitor = reitor;
    }

    public String getChefe() {
        return chefe;
    }

    public void setChefe(String chefe) {
        this.chefe = chefe;
    }

    public String getCoordenador() {
        return coordenador;
    }

    public void setCoordenador(String coordenador) {
        this.coordenador = coordenador;
    }

    public String getRepresentante() {
        return representante;
    }

    public void setRepresentante(String representante) {
        this.representante = representante;
    }    
    
}
